package it.unimib.campus.alessandro.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.unimib.campus.alessandro.backend.data.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
