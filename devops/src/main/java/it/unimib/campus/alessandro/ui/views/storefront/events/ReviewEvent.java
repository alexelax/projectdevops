package it.unimib.campus.alessandro.ui.views.storefront.events;

import com.vaadin.flow.component.ComponentEvent;
import it.unimib.campus.alessandro.ui.views.orderedit.OrderEditor;

public class ReviewEvent extends ComponentEvent<OrderEditor> {

	public ReviewEvent(OrderEditor component) {
		super(component, false);
	}
}