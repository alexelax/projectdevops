package it.unimib.campus.alessandro.ui.views.storefront.events;

import com.vaadin.flow.component.ComponentEvent;
import it.unimib.campus.alessandro.ui.views.orderedit.OrderItemsEditor;

public class ValueChangeEvent extends ComponentEvent<OrderItemsEditor> {

	public ValueChangeEvent(OrderItemsEditor component) {
		super(component, false);
	}
}