package it.unimib.campus.alessandro.ui.views.storefront.events;

import com.vaadin.flow.component.ComponentEvent;
import it.unimib.campus.alessandro.ui.views.orderedit.OrderItemEditor;

public class DeleteEvent extends ComponentEvent<OrderItemEditor> {
	public DeleteEvent(OrderItemEditor component) {
		super(component, false);
	}
}