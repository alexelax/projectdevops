package it.unimib.campus.alessandro.ui.utils.converters;

import com.vaadin.flow.templatemodel.ModelEncoder;
import it.unimib.campus.alessandro.ui.dataproviders.DataProviderUtil;
import it.unimib.campus.alessandro.ui.utils.FormattingUtils;

public class CurrencyFormatter implements ModelEncoder<Integer, String> {

	@Override
	public String encode(Integer modelValue) {
		return DataProviderUtil.convertIfNotNull(modelValue, FormattingUtils::formatAsCurrency);
	}

	@Override
	public Integer decode(String presentationValue) {
		throw new UnsupportedOperationException();
	}
}
