FROM tomcat:8.5-jre9
EXPOSE 8080
RUN rm -fr /usr/local/tomcat/webapps/ROOT
COPY target/my-starter-project-1.0-SNAPSHOT.war /usr/local/tomcat/webapps/ROOT.war
